<?php

/*
 * This file is part of [package name].
 *
 * (c) John Doe
 *
 * @license LGPL-3.0-or-later
 */

namespace Petschy90\TeamManager;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TeamManagerBundle extends Bundle
{
}
