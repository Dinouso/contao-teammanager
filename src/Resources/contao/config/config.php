<?php

use Petschy90\TeamManager\Models\TmTeamsModel;
use Petschy90\TeamManager\Models\TmPlayersModel;

# Models
$GLOBALS['TL_MODELS']['tl_tm_teams'] = TmTeamsModel::class;
$GLOBALS['TL_MODELS']['tl_tm_players'] = TmPlayersModel::class;

# CSS TODO
// if ('BE' === TL_MODE) {
//     $GLOBALS['TL_CSS'][] = 'vendor/petschy90/team-manager/src/Resources/css/backend.css';
// }

// Backend modules
array_insert($GLOBALS['BE_MOD'],1, array
(
    'teamManager' => array 
    (
        'tm_teams' => array 
        (
            'tables' => ['tl_tm_teams']
        ),
        'tm_players' => array 
        (
            'tables' => ['tl_tm_players']
        ),
        'tm_leagues' => array 
        (
            'tables' => ['tl_tm_leagues']
        ),
        'tm_games' => array 
        (
            'tables' => ['tl_tm_seasons','tl_tm_games']
        )
    )
));

// Frontend modules
$GLOBALS['FE_MOD']['miscellaneous']['teamManager'] = 'Petschy90\TeamManager\Module\TeamManagerModule';

?>