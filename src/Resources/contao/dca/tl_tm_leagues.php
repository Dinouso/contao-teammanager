<?php

$GLOBALS['TL_DCA']['tl_tm_leagues'] = array
(
	// Config
	'config' => array
	(     
        'label'                     => &$GLOBALS['TL_LANG']['tl_tm_leagues']['league'],
        'dataContainer'             => 'Table',
        'ctable'                    => array('tl_tm_seasons'),
        'enableVersioning'          => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid' => 'index'
            )
        )
	),

   	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
            'fields'                  => ['name'],
            'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit'
		),
		'label' => array
		(
            'fields'                  => ['name'],
            'showColumns'             => true,
            'label_callback'          => array('tl_tm_leagues', 'addIcon')
		),
		'global_operations' => array
		(

		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_leagues']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.svg'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_leagues']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.svg'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_leagues']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_leagues']['toggle'],
				'icon'                => 'visible.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_leagues']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.svg'
			)
		)
    ),
    
    // Palettes
	'palettes' => array
	(
		'default'                     => '{name_legend},name,image;{teams_legend},teams'
	),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_leagues']['name'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>''),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'image' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_leagues']['image'],
			'exclude'                 => true,
			'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'tl_class'=>'', 'extensions' => Config::get('validImageTypes')),
			'sql'                     => "binary(16) NULL"
		),
		'teams' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_leagues']['teams'],
            'exclude'                 => true,
            'sorting'                 => false,
            'inputType'               => 'listWizard',
			'load_callback'			  => array(array('tl_tm_leagues','getTeamsOfLeague')),
			'eval'					  => array('readonly' => true)
		),
    )
);  

/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @property 
 * 
 * @author
 */
class tl_tm_leagues extends Backend
{
    /**
	 * Add an image to each page in the tree
	 *
	 * @param array  $row
	 * @param string $label
	 *
	 * @return string
	 */
	public function addIcon($row, $label)
	{
        $file = \FilesModel::findByUuid($row['image']);
        $image = 'assets/contao/images/iconPLAIN.svg';

        if ($file) 
        {
            $image = $file->path;
        }

		return Image::getHtml($image,$row['name'],'style="max-height:18px; max-width:18px; margin-right: 5px;"') . $label;
	}

	public function getTeamsOfLeague($value, Contao\DataContainer $dc)
	{
		//var_dump($dc);

		//echo($dc->inId);

		//$objTeams = TmPlayersModel::findById($dc->inId);

		return array("Demodata","1. Mannschaft","2. Mannschhaft");
		//$objPlayers = TmPlayersModel::findAll();
		//deserialize($item['teams'])
	}

}

?>