<?php

$this->loadDataContainer('tl_tm_leagues');

$GLOBALS['TL_DCA']['tl_tm_games'] = array
(
	// Config
	'config' => array
	(
        'label'                       => &$GLOBALS['TL_LANG']['tl_tm_games']['games'],
		'dataContainer'               => 'Table',
		'ptable'                      => 'tl_tm_seasons',
		'switchToEdit'                => true,
		'enableVersioning'            => true,
		'onload_callback' => array
		(

		),
		'sql' => array
		(
			'keys' => array
			(
                'id' => 'primary',
                'pid' => 'index'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 4,
            'fields'                  => array('sorting'),
			'panelLayout'             => 'filter;search,limit',
			'headerFields'            => array('name', 'year', 'game'),
			'child_record_callback'   => array('tl_tm_games', 'addCteType')
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_games']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.svg'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_games']['copy'],
				'href'                => 'act=paste&amp;mode=copy',
				'icon'                => 'copy.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset()"'
			),
			'cut' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_games']['cut'],
				'href'                => 'act=paste&amp;mode=cut',
				'icon'                => 'cut.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset()"'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_games']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
				//'button_callback'     => array('tl_tm_games', 'deleteElement')
			),
			'toggle' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_games']['toggle'],
				'icon'                => 'visible.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
				//'button_callback'     => array('tl_tm_games', 'toggleIcon')
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_games']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.svg'
			)
		)
	),

	// Select
	'select' => array
	(
		'buttons_callback' => array
		(
			array('tl_tm_games', 'addAliasButton')
		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{name_legend},gameday,opponent,datetime,homeaway'
	),

	// Subpalettes
	'subpalettes' => array
	(

	),

	// Fields
	'fields' => array
	(
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
		'pid' => array
		(
			'foreignKey'              => 'tl_tm_leagues.id',
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'sorting' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'gameday' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_games']['gameday'],
			'search'                  => true,
            'sorting'                 => false,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>10, 'rgxp'=>'digit', 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        // 'opponent' => array
        // (
        //     'label'                   => &$GLOBALS['TL_LANG']['tl_tm_games']['opponent'],
        //     'search'                  => true,
		// 	'inputType'               => 'checkbox',
		// 	'foreignKey'			  => 'tl_tm_leagues.teams',
        //     'eval'                    => array('maxlength'=>255, 'multiple'=>true, 'tl_class'=>'w50'),
		// 	'sql'                     => "blob NULL",
		// 	'relation'                => array('type'=>'belongsToMany', 'load'=>'lazy')
        // )
        'opponent' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_games']['opponent'],
            'search'                  => true,
            'filter'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'datetime' => array
		(
			'exclude'                 => true,
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_games']['datetime'],
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
        ),
        'homeaway' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_games']['homeaway'],
            'search'                  => true,
            'filter'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'select',
            'options'                 => array('home','away'),
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(4) NOT NULL default ''"
        ),


	)
);

/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @property 
 * 
 * @author
 */
class tl_tm_games extends Backend
{
	/**
	 * Add the type of content element
	 *
	 * @param array $arrRow
	 *
	 * @return string
	 */
	public function addCteType($arrRow)
	{
		$class = 'limit_height';

		// // Remove the class if it is a wrapper element
		// if (in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['start']) || in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['separator']) || in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['stop']))
		// {
		// 	$class = '';

		// 	if (($group = $this->getContentElementGroup($arrRow['type'])) !== null)
		// 	{
		// 		$type = $GLOBALS['TL_LANG']['CTE'][$group] . ' (' . $type . ')';
		// 	}
		// }

		// // Add the group name if it is a single element (see #5814)
		// elseif (in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['single']))
		// {
		// 	if (($group = $this->getContentElementGroup($arrRow['type'])) !== null)
		// 	{
		// 		$type = $GLOBALS['TL_LANG']['CTE'][$group] . ' (' . $type . ')';
		// 	}
		// }

		// // Add the ID of the aliased element
		// if ($arrRow['type'] == 'alias')
		// {
		// 	$type .= ' ID ' . $arrRow['cteAlias'];
		// }

		// Add the protection status
		// if ($arrRow['protected'])
		// {
		// 	$type .= ' (' . $GLOBALS['TL_LANG']['MSC']['protected'] . ')';
		// }
		// elseif ($arrRow['guests'])
		// {
		// 	$type .= ' (' . $GLOBALS['TL_LANG']['MSC']['guests'] . ')';
		// }

		// Add the headline level (see #5858)
		// if ($arrRow['type'] == 'headline')
		// {
		// 	if (is_array($headline = StringUtil::deserialize($arrRow['headline'])))
		// 	{
		// 		$type .= ' (' . $headline['unit'] . ')';
		// 	}
		// }

		// Limit the element's height
		if (!Config::get('doNotCollapse'))
		{
			$class .=  ' h40';
		}

		$objModel = new ContentModel();
		$objModel->setRow($arrRow);

		return '
        <div class="cte_type ' . $key . '"> Game </div>
        <div class="' . trim($class) . '">
        ' . StringUtil::insertTagToSrc($this->getContentElement($objModel)) . '
        </div>' . "\n";
    }
    
	/**
	 * Return the edit article button
	 *
	 * @param array  $row
	 * @param string $href
	 * @param string $label
	 * @param string $title
	 * @param string $icon
	 * @param string $attributes
	 *
	 * @return string
	 */
	public function editGame($row, $href, $label, $title, $attributes)
	{
		$objPage = $this->Database->prepare("SELECT * FROM tl_page WHERE id=?")
								  ->limit(1)
								  ->execute($row['pid']);

		return $this->User->isAllowed(BackendUser::CAN_EDIT_ARTICLES, $objPage->row()) ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
	}
}

?>