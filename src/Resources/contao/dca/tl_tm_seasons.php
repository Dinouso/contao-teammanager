<?php

$GLOBALS['TL_DCA']['tl_tm_seasons'] = array
(
	// Config
	'config' => array
	(     
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        'ptable'                      => 'tl_tm_leagues',
		'ctable'                      => array('tl_tm_games'),
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid' => 'index'
            )
        )
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
            'mode'                    => 6,
            'panelLayout'             => 'filter;search',
            'flag'                    => 6
		),
		'label' => array
		(
			'fields'                  => array('name','year','game'),
			'format'                  => '%s <span style="padding-left:3px">%s</span> <span style="padding-left:3px">%s</span>',
		),
		'global_operations' => array
		(
			'toggleNodes' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['toggleAll'],
				'href'                => '&amp;ptg=all',
				'class'               => 'header_toggle',
				'showOnSelect'        => true
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_seasons']['edit'],
				'href'                => 'table=tl_tm_games',
				'icon'                => 'edit.svg',
				//'button_callback'     => array('tl_tm_seasons', 'editArticle')
			),
			'editheader' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_seasons']['editheader'],
				'href'                => 'act=edit',
				'icon'                => 'header.svg',
				//'button_callback'     => array('tl_tm_seasons', 'editHeader')
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_seasons']['copy'],
				'href'                => 'act=paste&amp;mode=copy',
				'icon'                => 'copy.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset()"',
				//'button_callback'     => array('tl_tm_seasons', 'copyArticle')
			),
			'cut' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_seasons']['cut'],
				'href'                => 'act=paste&amp;mode=cut',
				'icon'                => 'cut.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset()"',
				//'button_callback'     => array('tl_tm_seasons', 'cutArticle')
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_seasons']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
				//'button_callback'     => array('tl_tm_seasons', 'deleteArticle')
			),
			'toggle' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_seasons']['toggle'],
				'icon'                => 'visible.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
				//'button_callback'     => array('tl_tm_seasons', 'toggl,,eIcon')
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_seasons']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.svg'
			)
		)
	),
    
    // Palettes
	'palettes' => array
	(
		'default'                     => '{name_legend},name,year,active;{game_legend},game'
	),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_seasons']['name'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'year' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_seasons']['year'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'active' => [
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_seasons']['active'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'default'                 => 1,
            'eval'                    => ['tl_class'=>'w50'],
            'sql'                     => "char(1) NOT NULL default ''"
        ],
        'game' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_seasons']['game'],
            'search'                  => true,
            'filter'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'select',
            'options'                 => array('Fifa 20','Fifa 21'),
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        ),
    )
);  

/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @property 
 * 
 * @author
 */
class tl_tm_seasons extends Backend
{

}

?>