<?php

use Petschy90\TeamManager\Models\TmTeamsModel;
use Petschy90\TeamManager\Models\TmPlayersModel;

$GLOBALS['TL_DCA']['tl_tm_teams'] = array
(
	// Config
	'config' => array
	(     
        'dataContainer'             => 'Table',
        'enableVersioning'          => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
	),

   	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
            'fields'                  => ['teamname'],
            'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit'
		),
		'label' => array
		(
            'fields'                  => ['teamname'],
			'showColumns'             => true
		),
		'global_operations' => array
		(

		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_teams']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.svg'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_teams']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.svg'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_teams']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_teams']['toggle'],
				'icon'                => 'visible.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_teams']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.svg'
			)
		)
    ),
    
    // Palettes
	'palettes' => array
	(
		'default'                     => '{name_legend},teamname;{members_legend},members;{leagues_legend},leagues'
	),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'teamname' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_teams']['teamname'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'long'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'members_count' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_teams']['members_count'],
            'exclude'                 => true,
            'sorting'                 => false,
            'inputType'               => 'text',
            'eval'                    => ['maxlength'=>10, 'rgxp'=>'digit'],
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'members' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_teams']['members'],
            'exclude'                 => true,
            'sorting'                 => false,
            'inputType'               => 'listWizard',
			'load_callback'			  => array(array('tl_tm_teams','getPlayersOfTeam')),
			'eval'					  => array('readonly' => true)
		),
		'leagues' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_teams']['leagues'],
            'search'                  => true,
			'inputType'               => 'checkbox',
			'foreignKey'			  => 'tl_tm_leagues.name',
            'eval'                    => array('maxlength'=>255, 'multiple'=>true, 'tl_class'=>'w50'),
			'sql'                     => "blob NULL",
			'relation'                => array('type'=>'belongsToMany', 'load'=>'lazy')
        )

    )
);  

/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @property 
 * 
 * @author
 */
class tl_tm_teams extends Backend
{
	/**
	 * Return the IDs of the teams as array
	 *
	 * @return array
	 */
	public function getAllTeams()
	{
		$objTeams = TmTeamsModel::findAll();
	
		$return = array();

		if ($objTeams !== null)
		{
			while ($objTeams->next())
			{
				$return[$objTeams->id] = $objTeams->teamname;
			}
		}

		return $return;
	}

	public function getPlayersOfTeam($value, Contao\DataContainer $dc)
	{
		//var_dump($dc);

		//echo($dc->inId);

		//$objTeams = TmPlayersModel::findById($dc->inId);

		return array("Demodata","petsch","lamoos");
		//$objPlayers = TmPlayersModel::findAll();
		//deserialize($item['teams'])
	}

}

?>