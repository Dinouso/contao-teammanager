<?php

$GLOBALS['TL_DCA']['tl_tm_players'] = array
(
	// Config
	'config' => array
	(     
        'dataContainer'             => 'Table',
        'enableVersioning'          => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
            )
        )
	),

   	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
            'fields'                  => ['nickname'],['kit_number'],
            'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit'
		),
		'label' => array
		(
            'fields'                  => array('nickname','kit_number'),
			'showColumns'             => true
		),
		'global_operations' => array
		(

		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_players']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.svg'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_players']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.svg'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_players']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_players']['toggle'],
				'icon'                => 'visible.svg',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_tm_players']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.svg'
			)
		)
    ),
    
    // Palettes
	'palettes' => array
	(
        '__selector__'                => array('loan'),
		'default'                     => '{name_legend},nickname,kit_number,join_date,image;{person_legend},name,surname;{accounts_legend},psn_id,discord_name;{loan_legend:hide},loan;{teams_legend},teams'
    ),
    
    // Subpalettes
	'subpalettes' => array
	(
		'loan'                        => 'loan_club'
	),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
        'nickname' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['nickname'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'kit_number' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['kit_number'],
			'search'                  => true,
            'sorting'                 => false,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>10, 'rgxp'=>'digit', 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'join_date' => array
		(
			'exclude'                 => true,
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['join_date'],
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
        ),
        'image' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['image'],
			'exclude'                 => true,
			'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'tl_class'=>'long clr', 'extensions' => Config::get('validImageTypes')),
			'sql'                     => "binary(16) NULL"
        ),
        'name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['name'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'surname' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['surname'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'psn_id' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['psn_id'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'discord_name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['discord_name'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'teams' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['teams'],
            'search'                  => true,
			'inputType'               => 'checkbox',
			'foreignKey'			  => 'tl_tm_teams.teamname',
            'eval'                    => array('maxlength'=>255, 'multiple'=>true, 'tl_class'=>'w50'),
			'sql'                     => "blob NULL",
			'relation'                => array('type'=>'belongsToMany', 'load'=>'lazy')
        ),
        'loan' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['loan'],
			'exclude'                 => true,
			//'filter'                  => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''"
        ),
        'loan_club' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_tm_players']['loan_club'],
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
        
    )
);    

/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @property 
 * 
 * @author
 */
class tl_tm_players extends Backend
{

}

?>