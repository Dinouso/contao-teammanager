<?php

namespace Petschy90\TeamManager\Models;

use Contao\Model;

/**
 * Reads and writes teams
 *
 *
 * @author
 */
class TmPlayersModel extends \Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_tm_players';
}