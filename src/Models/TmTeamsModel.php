<?php

namespace Petschy90\TeamManager\Models;

use Contao\Model;

/**
 * Reads and writes teams
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $teamname
 * @property integer $members_count
 *
 * @method static TmTeamsModel|null findById($id, array $opt=array())
 * @method static TmTeamsModel|null findByPk($id, array $opt=array())
 * @method static TmTeamsModel|null findByTeamname($val, array $opt=array())
 * @method static TmTeamsModel|null findOneBy($col, $val, array $opt=array())
 *
 * @method static Model\Collection|TmTeamsModel[]|TmTeamsModel|null findByTstamp($val, array $opt=array())
 * @method static Model\Collection|TmTeamsModel[]|TmTeamsModel|null findMultipleByIds($val, array $opt=array())
 * @method static Model\Collection|TmTeamsModel[]|TmTeamsModel|null findBy($col, $val, array $opt=array())
 * @method static Model\Collection|TmTeamsModel[]|TmTeamsModel|null findAll(array $opt=array())
 *
 * @method static integer countById($id, array $opt=array())
 * @method static integer countByTstamp($val, array $opt=array())
 * @method static integer countByTeamname($val, array $opt=array())
 *
 * @author
 */
class TmTeamsModel extends \Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_tm_teams';
}