<?php

/*
 * This file is part of [package name].
 *
 * (c) John Doe
 *
 * @license LGPL-3.0-or-later
 */

namespace Petschy90\TeamManager\Tests;

use Petschy90\TeamManager\TeamManagerBundle;
use PHPUnit\Framework\TestCase;

class TeamManagerBundleTest extends TestCase
{
    public function testCanBeInstantiated()
    {
        $bundle = new TeamManagerBundle();

        $this->assertInstanceOf('Petschy90\TeamManager\TeamManagerBundle', $bundle);
    }
}
